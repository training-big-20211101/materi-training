# Materi Training

1.  Understanding Kubernetes Architecture
1.1.  Introduction to Docker
1.2.  Introduction to Kubernetes
1.3.  Kubernetes Concepts
1.4.  Diving into Kubernetes Architecture in Depth
1.5.  Kubernetes Runtimes
1.6.  Continuous Integration and Deployment

2.  Creating Kubernetes Clusters
2.1.  Creating a Single-node Cluster with Minikube
2.2.  Creating a Multi-node Cluster with KinD
2.3.  Creating a Multi-node Cluster with k3d
2.4.  Comparing MiniKube, KinD, and k3d
2.5.  Creating Clusters in the Cloud (GCP, AWS, Azure)
2.6.  Creating a Bare-metal Cluster from Scratch

3.  High Availability and Reliability
3.1.  High Availability Concepts
3.2.  High Availability Best Practice
3.3.  High Availability, Scalability, and Capacity Planning
3.4.  Live Cluster Updates
3.5.  Large Cluster Performance, Cost, and Design Trade-offs

4.  Securing Kubernetes
4.1.  Understanding Kubernetes Security Challenges
4.2.  Hardening Kubernetes
4.3.  Running a Multi-user Cluster
 
5.  Using Kubernetes Resources in Practice
5.1.  Designing a Hue Platform
5.2.  Using Kubernetes to Build the Hue Platform
5.3.  Separating Internal and External Services
5.4.  Advanced Scheduling 
5.5.  Using Namespaces to Limit Access
5.6.  Using Customization for Hierarchical Cluster Structures
5.7.  Launching Jobs
5.8.  Mixing Non-Cluster Components
5.9.  Evolving the Hue Platform with Kubernetes

6.  Managing Storage
6.1.  Persistent Volumes Walkthrough
6.2.  Public Cloud Storage Volume Types – GCE, AWS, and Azure
6.3.  GlusterFS and Ceph Volumes in Kubernetes
6.4.  Flocker as a Clustered Container Data Volume Manager
6.5.  Integrating Enterprise Storage into Kubernetes
6.6.  Projecting Volumes
6.7.  Using out-of-tree Volume Plugins with FlexVolume
6.8.  The Container Storage Interface

7.  Running Stateful Applications with Kubernetes
7.1.  Stateful Versus Stateless Applications in Kubernetes
7.2.  Shared Environment Variables Versus DNS Record for Discovery
7.3.  Running a Cassandra Cluster in Kubernetes

8.  Deploying and Updating Applications
8.1.  Horizontal Pod Autoscaling
8.2.  Performing Rolling Updates with Autoscaling
8.3.  Handling Scarce Resources with Limits and Quotas
8.4.  Choosing and Managing the Cluster Capacity
8.5.  Pushing the Envelope with Kubernetes

9.  Packaging Applications
9.1.  Understanding Helm
9.2.  Using Helm
9.3.  Creating Your Own Charts

10.  Exploring Advanced Networking
10.1.  Understanding the Kubernetes Networking Model
10.2.  Kubernetes Networking Solutions
10.3.  Using Network Policies Effectively
10.4.  Load Balancing Options
10.5.  Writing Your Own CNI Plugin

11.  Running Kubernetes on Multiple Clouds and Cluster Federation
11.1.  The history of cluster federation on Kubernetes 
11.2.  Understanding cluster federation
11.3.  Managing a Kubernetes Cluster Federation
11.4.  Introducing the Gardener project

12.  Serverless Computing on Kubernetes
12.1.  Understanding serverless computing
12.2.  Serverless Kubernetes in the cloud
12.3.  Knative
12.4.  Kubernetes FaaS Frameworks

13.  Monitoring Kubernetes Clusters
13.1.  Understanding observability
13.2.  Logging with Kubernetes
13.3.  Collecting metrics with Kubernetes
13.4.  Distributed tracing with Jaeger
13.5.  Troubleshooting problems

14.  Utilizing Service Meshes
14.1.  What is a service mesh?
14.2.  Choosing a service mesh
14.3.  Incorporating Istio into your Kubernetes cluster

15.  Extending Kubernetes
15.1.  Working with the Kubernetes API
15.2.  Extending the Kubernetes API
15.3.  Writing Kubernetes plugins
15.4.  Employing access control webhooks

16.  The Future of Kubernetes
16.1.  The Kubernetes momentum
16.2.  The rise of managed Kubernetes platforms
16.3.  Upcoming trends